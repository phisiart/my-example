#include <fstream>
#include <string>
#include <iostream>
#include <sstream>

int main(int argc, char *argv[])
{
    std::string file_name_prefix = "mnist";

    if (argc >= 2) {
        file_name_prefix = std::string(argv[1]);
        std::cout << "file name = " << file_name_prefix << std::endl;
    }

    
    std::ifstream is("mnist");
    int nlines = 8000000;
    int nslices = 4;

    int nlines_per_slice = nlines / nslices;
    int ifile = 0;

    std::string line;
    line.reserve(10000);
    int cnt = 0;
    
    std::ofstream *os = NULL;

    while (std::getline(is, line)) {
        if (cnt % nlines_per_slice == 0) {
            // open a new file
            std::ostringstream file_name;
            file_name << file_name_prefix << ifile;
            std::cout << "writing to file " << file_name.str() << std::endl;
            if (os != NULL) {
                delete os;
            }
            os = new std::ofstream(file_name.str().c_str());
            ifile++;
        }

        if (cnt % 10000 == 0) {
            std::cout << "cnt = " << cnt << std::endl;
        }
        
        *os << line << std::endl;
        
        cnt++;
    }

    std::cout << "finished. cnt = " << cnt << std::endl;

    return 0;

}