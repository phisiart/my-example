import org.apache.log4j.{Level, Logger};
import org.apache.spark.mllib.regression.LabeledPoint;
import org.apache.spark.mllib.util.MLUtils;
import org.apache.spark.rdd.RDD;
import org.apache.spark.storage.StorageLevel;
import org.apache.spark.{SparkConf, SparkContext};

/**
  * Created by johnson on 6/10/16.
  */
object PS {
    def GetGradient(w: Array[Double], x: Array[Double], y: Double): Array[Double] = {
        val coef = y - GetSigmoid(w, x)
        x.map(_ * coef)
    }

    def GetSigmoid(w: Array[Double], x: Array[Double]): Double = {
        1.0 / (1.0 + math.exp(-dot(w, x)))
    }

    case class Timer(var last: Long = System.currentTimeMillis()) {
        var cnt = 0
        var avg = 0.0

        def reset() = {
            this.last = System.currentTimeMillis()
        }

        def print() = {
            val curr = System.currentTimeMillis()
            val time = curr - this.last
            avg = (avg * cnt + time) / (cnt + 1)
            cnt += 1
            this.last = curr

            println(s"[$cnt] Time used: $time, avg = $avg")
        }
    }

    def add(v1 : Array[Double], v2 : Array[Double]) : Array[Double] = {
        val len = math.min(v1.length, v2.length)
        val ret = new Array[Double](len)
        for (i <- 0 until len) {
            ret(i) = v1(i) + v2(i)
        }
        ret
        // (v1, v2).zipped.map(_ - _)
    }

    // vector subtraction
    def sub(v1 : Array[Double], v2 : Array[Double]) : Array[Double] = {
        val len = math.min(v1.length, v2.length)
        val ret = new Array[Double](len)
        for (i <- 0 until len) {
            ret(i) = v1(i) - v2(i)
        }
        ret
        // (v1, v2).zipped.map(_ - _)
    }

    // vector multiplication
    def mult(v : Array[Double], a : Double) : Array[Double] = {
        val ret = new Array[Double](v.length)
        for (i <- v.indices) {
            ret(i) = v(i) * a
        }
        ret
        // v.map(_ * a)
    }

    // vector dot product
    def dot(v1 : Array[Double], v2 : Array[Double]) : Double = {
        var ret = 0.0
        val len = math.min(v1.length, v2.length)
        for (i <- 0 until len) {
            ret += v1(i) * v2(i)
        }
        ret
        // (v1, v2).zipped.map(_ * _).sum
    }

    def main(args: Array[String]) {
//        Logger.getLogger("org").setLevel(Level.OFF)
//        Logger.getLogger("akka").setLevel(Level.OFF)

        val conf = new SparkConf().setAppName("LR")
//            .setMaster("local[8]")

        val sc = new SparkContext(conf)

        val data_slices = (0 until 4)
            .map(idx => {
                val data_slice = MLUtils
                    .loadLibSVMFile(sc, "mnist" + idx)
                    .map(point => (point.features.toArray, point.label))

                data_slice
            })

        // val data_slices_ = data_slices.map(_.collect())

        val dim = 784
        val zero = new Array[Double](dim)
        val nexamples = 60000
        val learning_rate = 0.001 / nexamples
        val niters = 100

        var w = zero
        val timer = new Timer
        var cnt = 0

        class GradientComputer(data: RDD[(Array[Double], Double)]) extends Runnable {
            override def run(): Unit = {
                while (true) {
                    val current_w = w.synchronized {
                        w
                    }
                    
                    // val gradients = data.map { case (x, y) => GetGradient(current_w, x, y) }
                    // val sum = gradients.reduce(add)

                    val sum = data
                        .map { case (x, y) => GetGradient(current_w, x, y) }
                        .reduce(add)

                    val delta = mult(sum, learning_rate)

                    w.synchronized {
                        cnt += 1
                        w = sub(w, delta)
                        timer.print()
                        if (cnt >= niters * 4) {
                            return
                        }
                    }
                }
            }
        }

        val runnables = data_slices.map(slice => new GradientComputer(slice))
        val threads = runnables.map(new Thread(_))
        threads.foreach(_.start())
        threads.foreach(_.join())
    }
}
