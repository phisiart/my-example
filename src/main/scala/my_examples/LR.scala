import org.apache.spark._;
import org.apache.spark.SparkContext;
import org.apache.spark.mllib.regression.LabeledPoint;
import org.apache.spark.mllib.util.MLUtils;
import org.apache.spark.rdd.RDD;
import org.apache.spark.storage.StorageLevel;

object LR {
    def GetGradient(w: Array[Double], x: Array[Double], y: Double): Array[Double] = {
        val coef = y - GetSigmoid(w, x)
        x.map(_ * coef)
    }

    def GetSigmoid(w: Array[Double], x: Array[Double]): Double = {
        1.0 / (1.0 + math.exp(-dot(w, x)))
    }

    case class Timer(var last: Long = System.currentTimeMillis()) {
        var cnt = 0
        var avg = 0.0

        def reset() = {
            this.last = System.currentTimeMillis()
        }

        def print() = {
            val curr = System.currentTimeMillis()
            val time = curr - this.last
            avg = (avg * cnt + time) / (cnt + 1)
            cnt += 1
            this.last = curr

            println(s"[$cnt] Time used: $time, avg = $avg")
        }
    }

    def add(v1 : Array[Double], v2 : Array[Double]) : Array[Double] = {
        val ret = new Array[Double](v1.length)
        for (i <- v1.indices) {
            ret(i) = v1(i) + v2(i)
        }
        ret
        // (v1, v2).zipped.map(_ - _)
    }

    // vector subtraction
    def sub(v1 : Array[Double], v2 : Array[Double]) : Array[Double] = {
        val ret = new Array[Double](v1.length)
        for (i <- v1.indices) {
            ret(i) = v1(i) - v2(i)
        }
        ret
        // (v1, v2).zipped.map(_ - _)
    }

    // vector multiplication
    def mult(v : Array[Double], a : Double) : Array[Double] = {
        val ret = new Array[Double](v.length)
        for (i <- v.indices) {
            ret(i) = v(i) * a
        }
        ret
        // v.map(_ * a)
    }

    // vector dot product
    def dot(v1 : Array[Double], v2 : Array[Double]) : Double = {
        var ret = 0.0
        for (i <- v1.indices) {
            ret += v1(i) * v2(i)
        }
        ret
        // (v1, v2).zipped.map(_ * _).sum
    }

    def main(args: Array[String]) = {
        val conf = new SparkConf().setAppName("LR")
        val sc = new SparkContext(conf)
        val points: RDD[LabeledPoint] = MLUtils.loadLibSVMFile(sc, "mnist")
        val data: RDD[(Array[Double], Double)] =
            points.map(point => (point.features.toArray, point.label))
                  //.cache()
                  //.persist(StorageLevel.MEMORY_AND_DISK_2)
        
        val dim = 780
        val zero = new Array[Double](dim)
        val nexamples = 60000
        val learning_rate = 0.001 / nexamples
        val niters = 100

        var w = zero

        val timer = new Timer
        for (iter <- 1 to niters) {
            val gradients = data.map { case (x, y) => GetGradient(w, x, y) }
            val sum = gradients.reduce(add)
            val delta = mult(sum, learning_rate)
            w = sub(w, delta)
            timer.print()
        }
    }
}